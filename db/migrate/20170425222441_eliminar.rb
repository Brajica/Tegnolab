class Eliminar < ActiveRecord::Migration[5.0]
  def change
    remove_column :ratings, :created_at
    remove_column :ratings, :updated_at
    remove_column :ratings, :unique
  end
end
