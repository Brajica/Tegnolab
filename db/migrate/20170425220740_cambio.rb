class Cambio < ActiveRecord::Migration[5.0]
  def change
    rename_column :ratings, :valoracion, :valor
  end
end
