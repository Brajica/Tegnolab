class CreateUniques < ActiveRecord::Migration[5.0]
  def change
    create_table :uniques do |t|
      t.string :unique

      t.timestamps
    end
  end
end
