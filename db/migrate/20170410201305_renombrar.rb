class Renombrar < ActiveRecord::Migration[5.0]
  def change
    rename_column :ratings, :valoracions_id, :valoracion_id
  end
end
