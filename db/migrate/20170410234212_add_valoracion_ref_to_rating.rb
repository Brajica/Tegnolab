class AddValoracionRefToRating < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :ratings, column: :valoracion_id
    remove_column  :ratings, :valoracion_id
    add_reference :ratings, :valoracion, foreign_key: true
  end
end
