class ValoracionsController < ApplicationController
  before_action :set_rating, only: [ :destroy]
  def index
  @valoraciones=Valoracion.all
  end
  def new
   @valoracion=Valoracion.new
  end
  def create
   @valoracion = Valoracion.new(parametros)
   respond_to do |format|
       if @valoracion.save
         format.html { redirect_to root_path, notice: 'Valoracion hecha' }
         format.json { render :show, status: :created, location: @client }
       else
         format.html { render :new }
         format.json { render json: @client.errors, status: :unprocessable_entity }
       end
   end
 end

 def destroy
a=Rating.where(:valoracions_id => @valoracion.id).destroy_all
   @valoracion.destroy
   respond_to do |format|
     format.html { redirect_to valoracions_url, notice: @valoracion.id }
     format.json { head :no_content }
   end
 end
  private
   def parametros
      params.require(:valoracion).permit(:nombre)
   end
   def set_rating
     @valoracion = Valoracion.find(params[:id])
   end
end
