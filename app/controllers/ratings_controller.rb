class RatingsController < ApplicationController
  before_action :set_rating, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only:[:create]

  # GET /ratings
  # GET /ratings.json
  def index
    @ratings = Rating.all
  end

  # GET /ratings/1
  # GET /ratings/1.json
  def show
  end

  # GET /ratings/new
  def new
    @rating = Rating.new
    @valoraciones=Valoracion.all
  @app=App.find(params[:id])
  @p=params[:id]
  end

  # GET /ratings/1/edit
  def edit
  end

  # POST /ratings
  # POST /ratings.json
  def create
    val=Valoracion.all
    c=0
    u=Unique.find_by_unique(params[:rating][:unique])
    if u.nil?
      unique=Unique.create(:unique => params[:rating][:unique])
        f=params[:rating][:valoracion]
            val.each do |app|
              sql=  Rating.create(:user_id => params[:rating][:user_id],
                            :app_id=> params[:rating][:app_id],
                            :valoracion_id => app.id,
                            :valor => f[app.nombre]
                            )
                            if sql.save
                              c=c+1
                            end

            end
            if c==val.size
              redirect_to apps_url
                 flash[:notice] ="Muchas gracias tu calificacion nos sirven "
            end
      else
        redirect_to :back
        flash[:notice] ="No puedes calificar ds veces la misma aplicacion"
    end
  end

  # PATCH/PUT /ratings/1
  # PATCH/PUT /ratings/1.json
  def update
    respond_to do |format|
      if @rating.update(rating_params)
        format.html { redirect_to @rating, notice: 'Rating was successfully updated.' }
        format.json { render :show, status: :ok, location: @rating }
      else
        format.html { render :edit }
        format.json { render json: @rating.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ratings/1
  # DELETE /ratings/1.json
  def destroy
    @rating.destroy
    respond_to do |format|
      format.html { redirect_to ratings_url, notice: 'Rating was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rating
      @rating = Rating.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rating_params
      params.require(:rating).permit(:app_id, :user_id,:valoracion_id,:valor)
    end
end
